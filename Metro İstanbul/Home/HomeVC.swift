//
//  HomeVC.swift
//  Metro İstanbul
//
//  Created by fatih inan on 10.07.2019.
//  Copyright © 2019 spexco. All rights reserved.
//

import UIKit

public class HomeVC: UIViewController {
    
    public var sView = HomeView()
    
    open override func loadView() {
        view = sView
    }
    
}
