//
//  HomeView.swift
//  Metro İstanbul
//
//  Created by fatih inan on 10.07.2019.
//  Copyright © 2019 spexco. All rights reserved.
//

import SPXBaseFramework
import SPXUIToolsFramework

public class HomeView: SPXView {
    
    override public func buildSubviews() {
        backgroundColor = UIColor.red
    }
    
}
